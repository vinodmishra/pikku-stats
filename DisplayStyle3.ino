/*----------------------------------------------------------------------------------
  DISPLAY STYLE 3
  ----------------------------------------------------------------------------------*/

void DisplayStyle3() {
    //-------------------------------------------Clearing Box ----------------------------------------------------

    //Clearing Boxes, eg: display.fillRect(<X>, ^Y^, W, H, Color);*/
    display.fillRect(0, 0, 128, 64, BLACK);  // Clear entire screen for testing

    //---------------------------------------- Static Background ----------------------------------------------------

    display.setTextSize(2);  //set background txt font size
    display.setCursor(1, 12);
    display.println("CPU");
    display.setCursor(1, 38);
    display.println("GPU");
    display.setTextSize(1);  //set background txt font size

    //---------------------------------------CPU & GPU Hardware ID--------------------------------------------------

    /*CPU & GPU Hardware ID*/
    display.setTextSize(0);  // string font size
    display.println("");
    display.setTextSize(1);
    display.setCursor(0, 1);
    display.println(getCpuName());
    display.setTextSize(0);
    display.setCursor(0, 28);
    display.println(getGpuName());

    //------------------------------------------ CPU Freq -------------------------------------------------

    /*CPU  Freq Display*/
    display.setTextSize(2);
    display.setCursor(42, 12);
    display.print(getValue("CC"));
    display.setTextSize(1);
    display.print("MHz");

    //------------------------------------------ CPU Temp -------------------------------------------------

    /*CPU Temp Display*/
    display.setTextSize(1);
    display.setCursor(105, 22);
    display.print(getValue("CT"));
    display.setTextSize(1);
#ifndef noDegree
    display.print((char)247);  //Degrees Symbol
#endif
    display.print("C ");  // Centigrade Symbol

    //------------------------------------------ GPU Freq/Temp -------------------------------------------------

    /* GPU temp V's GPU freq to check for throttling and max 'GPU Boost' */

    /*GPU Core Freq Display*/
    display.setTextSize(2);
    display.setCursor(42, 38);
    display.print(getValue("GCC"));
    display.setTextSize(1);
    display.print("MHz");

    /*GPU VRAM Freq Display*/
    display.setCursor(1, 56);
    display.print("VRAM:  ");
    display.print(getValue("GMC"));
    display.print("MHz");

    /*GPU Core Temp Display*/
    display.setTextSize(1);
    display.setCursor(105, 48);
    display.print(getValue("GT"));
    display.setTextSize(1);

#ifndef noDegree
    display.print((char)247);  //Degrees Symbol
#endif
    display.print("C ");  // Centigrade Symbol

    display.fillRect(118, 0, 10, 10, BLACK);  

    display.display();

    oledDraw = 1;
}
