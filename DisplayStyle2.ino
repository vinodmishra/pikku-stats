/*----------------------------------------------------------------------------------
  DISPLAY STYLE 2
  ----------------------------------------------------------------------------------*/

void DisplayStyle2() {
    //-------------------------------------------Clearing Box ----------------------------------------------------

    //Clearing Boxes, eg: display.fillRect(<X>, ^Y^, W, H, Color);*/
    display.fillRect(0, 0, 128, 64, BLACK);  // Clear entire screen for testing

    //---------------------------------------- Static Background ----------------------------------------------------

    display.setTextSize(2);  //set background txt font size
    display.setCursor(1, 12);
    display.println("CPU");
    display.setCursor(1, 38);
    display.println("GPU");
    display.setTextSize(1);  //set background txt font size

    //---------------------------------------CPU & GPU Hardware ID--------------------------------------------------

    /*CPU & GPU Hardware ID*/
    display.setTextSize(0);  // string font size
    display.println("");
    display.setTextSize(1);
    display.setCursor(0, 1);
    display.println(getCpuName());
    display.setTextSize(0);
    display.setCursor(0, 28);
    display.println(getGpuName());

    //------------------------------------------ CPU Freq -------------------------------------------------

    /*CPU Core Freq*/
    display.setTextSize(2);
    display.setCursor(42, 12);
    display.print(getValue("CC"));
    display.setTextSize(1);
    display.print("MHz");

    //------------------------------------------ CPU Temp -------------------------------------------------
    /*CPU TEMPERATURE*/
    display.setTextSize(1);
    display.setCursor(105, 22);
    display.print(getValue("CT"));
    display.setTextSize(1);
#ifndef noDegree
    display.print((char)247);  //Degrees Symbol
#endif
    display.print("C ");  // Centigrade Symbol

    //------------------------------------------ GPU Freq/Temp -------------------------------------------------

    /*GPU Core Freq*/

    display.setTextSize(1);
    display.setCursor(42, 38);
    display.print("Core  :");
    display.print(getValue("GCC"));
    display.setTextSize(1);
    display.print("MHz");  // Centigrade Symbol
    display.setCursor(42, 46);
    display.print("VRAM  :");
    display.print(getValue("GMC"));
    display.print("MHz");
    display.setCursor(42, 54);
    display.print("Shader:");
    display.print(getValue("GSC"));
    display.print("MHz");

    /*GPU TEMPERATURE*/
    display.setTextSize(1);
    display.setCursor(1, 56);
    display.print(getValue("GT"));
    display.setTextSize(1);

#ifndef noDegree
    display.print((char)247);  //Degrees Symbol
#endif
    display.print("C ");  // Centigrade Symbol

    display.fillRect(118, 0, 10, 10, BLACK);  

    display.display();

    oledDraw = 1;
}
