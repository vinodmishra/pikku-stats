/*----------------------------------------------------------------------------------
  DISPLAY STYLE 4
  ----------------------------------------------------------------------------------*/

void DisplayStyle4() {
    //-------------------------------------------Clearing Box ----------------------------------------------------

    //Clearing Boxes, eg: display.fillRect(<X>, ^Y^, W, H, Color);*/
    display.fillRect(0, 0, 128, 64, BLACK);  // Clear entire screen

    //--------------------------------------- Display Background ----------------------------------------------------

    /* OLED Background */
    display.setTextSize(1);
    display.setCursor(0, 1);
    display.println("WATER");
    display.setCursor(45, 1);
    display.println("VRM");
    display.setCursor(87, 1);
    display.println("MB");
    display.setCursor(0, 30);
    display.println("OS DRIVE: ");
    display.setCursor(55, 30);
    display.print(getValue("OSDT"));
#ifndef noDegree
    display.print((char)247);  //Degrees Symbol
#endif
    display.print("C    ");  // Centigrade Symbol
    display.setCursor(0, 38);
    display.println("INTAKE: ");
    display.setCursor(0, 46);
    display.println("EXHAUST: ");
    display.setCursor(1, 54);
    display.println("WATER PUMP: ");
    display.setTextSize(2);  //set background txt font size
    display.setCursor(1, 12);
    display.print(getValue("WOT"));
    display.setTextSize(1);

#ifndef noDegree
    display.print((char)247);  //Degrees Symbol
#endif
    display.print("C ");  // Centigrade Symbol

    display.setTextSize(2);  //set background txt font size
    display.print(getValue("VRMT"));
    display.setTextSize(1);

#ifndef noDegree
    display.print((char)247);  //Degrees Symbol
#endif
    display.print("C ");  // Centigrade Symbol
    
    display.setTextSize(2);  //set background txt font size
    display.print(getValue("MBT"));
    display.setTextSize(1);
    
#ifndef noDegree
    display.print((char)247);  //Degrees Symbol
#endif
    display.print("C ");  // Centigrade Symbol
    
    display.setCursor(45, 38);
    display.print(getValue("FI") + " RPM");
    display.setCursor(50, 46);
    display.print(getValue("FO") + " RPM");
    display.setCursor(67, 54);
    display.print(getValue("WP") + " RPM");

    display.fillRect(118, 0, 10, 10, BLACK);  

    display.display();

    oledDraw = 1;
}
