/*----------------------------------------------------------------------------------
  DISPLAY STYLE 1
  ----------------------------------------------------------------------------------*/

void DisplayStyle1() {
    //-------------------------------------------Clearing Box ----------------------------------------------------

    //Clearing Boxes, eg: display.fillRect(<X>, ^Y^, W, H, Color);*/
    display.fillRect(0, 0, 128, 64, BLACK);  // Clear entire screen

    //--------------------------------------- Display Background ----------------------------------------------------

    /* OLED Background */
    display.setTextSize(2);  //set background txt font size
    display.setCursor(1, 12);
    display.println("CPU");
    display.setCursor(1, 38);
    display.println("GPU");
    display.setTextSize(1);  //set background txt font size
    display.setCursor(1, 56);
    display.println("SYSRAM");

    //---------------------------------------CPU & GPU Hardware ID----------------------------------------------------

    display.setTextSize(0);  // string font size
    display.println("");
    display.setTextSize(1);
    display.setCursor(0, 1);
    display.println(getCpuName());
    display.setTextSize(0);
    display.setCursor(0, 28);
    display.println(getGpuName());

    //------------------------------------------ CPU Load/Temp -------------------------------------------------
    /*CPU TEMPERATURE*/
    display.setTextSize(2);
    display.setCursor(42, 12);
    display.print(getValue("CT"));
    display.setTextSize(1);

#ifndef noDegree
    display.print((char)247);  //Degrees Symbol
#endif
    display.print("C ");  // Centigrade Symbol

    /*CPU LOAD, ALL CORES*/
    display.setTextSize(2);
    display.print(" " + getValue("CL"));
    display.setTextSize(1);
    display.println("%");  // Small Percent Symbol

    //------------------------------------------ GPU Load/Temp -------------------------------------------------

    /*GPU TEMPERATURE*/
    display.setTextSize(2);
    display.setCursor(42, 38);
    display.print(getValue("GT"));
    display.setTextSize(1);

#ifndef noDegree
    display.print((char)247);  //Degrees Symbol
#endif
    display.print("C ");  // Centigrade Symbol

    /*GPU LOAD*/
    display.setTextSize(2);
    display.print(" " + getValue("GL"));
    display.setTextSize(1);
    display.println("%");  // Small Percent Symbol

    //----------------------------------------SYSTEM RAM USAGE---------------------------------------------------

    /*RAM USAGE*/
    display.setTextSize(1);  //set background txt font size
    display.setCursor(42, 56);
    display.print(getValue("RU") + "GB");

    display.fillRect(118, 0, 10, 10, BLACK);  

    display.display();

    oledDraw = 1;
}
