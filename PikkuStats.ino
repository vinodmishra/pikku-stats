/*
  PikkuStats - Version 0.1 
  Vinod Mishra

  ----------------------------------------------------------------------------------
  ATTRIBUTION
  ----------------------------------------------------------------------------------
  Heavily based on 
  GNATSTATS PC Performance Monitor - Version 1.x  Rupert Hirst & Colin Conway © 2016
  http://tallmanlabs.com  & http://runawaybrainz.blogspot.com/

  ----------------------------------------------------------------------------------
  LICENSE
  ----------------------------------------------------------------------------------
  Attribution-NonCommercial-ShareAlike  CC BY-NC-SA

  This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.

  https://creativecommons.org/licenses/

  ----------------------------------------------------------------------------------
  NOTES
  ----------------------------------------------------------------------------------
  - Only ATmega32U4 boards are supported
  - Only supports SH1106 or SSD1306 i2c displays supported
  - Needs the counterpart desktop application Wee Hardware Stat Server available at https://gitlab.com/vinodmishra/wee-hardware-stat-server which uses LibreHardwareMonitor lib to detect the hardware.
  - Does not support integrated GPUs. 

  ----------------------------------------------------------------------------------
  NOTES
  ----------------------------------------------------------------------------------
    Version 0.1   : Refactor and reorganisation to clean up duplicate code and avoid doing extra procesing when getting incoming data.
                    Switched to a new data format which uses WeeHardwareStatmonitor instead which is easier to parse.
                    Removed Neopixel as it frees up some memory and I have no need for it at the moment. 

  ----------------------------------------------------------------------------------
  LIBRARIES
  ----------------------------------------------------------------------------------
  Adafruit SSD1306 library
  https://github.com/adafruit/Adafruit_SSD1306

  Adafruit library ported to the SH1106
  https://github.com/badzz/Adafruit_SH1106 

  Adafruit GFX Library
  https://github.com/adafruit/Adafruit-GFX-Library
*/
#include <Adafruit_GFX.h>
#include <SPI.h>
#include <Wire.h>

#include "bitmap.h"

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#define CODE_VERS "0.1"  // Code version number
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/*----------------------------------------------------------------------------------
  CONFIGURATION OPTIONS
  ----------------------------------------------------------------------------------
  ----------------------------------
  Pins Reference
  ----------------------------------
  SDA: D2, SCL: D3
  ----------------------------------*/

//----------------------------------- OLED Setup ----------------------------------------

/*Uncomment the correct OLED display type, uncomment only one!!!*/
#define OLED_SSD1306
//#define OLED_SH1106

/* Uncomment the initialize the I2C address , uncomment only one, If you get a totally blank screen try the other*/
#define i2c_Address 0x3c  //initialize with the I2C addr 0x3C Typically eBay OLED's
//#define i2c_Address 0x3d //initialize with the I2C addr 0x3D Typically Adafruit OLED's

/*Rotate the display at the start:  0 or 2  (0, 180 degrees)*/
#define rotateScreen 2

/* Uncomment below, to enable positive and negative screen cycler */
//#define enableInvertscreen

/* Uncomment below, to take out small degree symbol for better spacing
   when hitting 100% cpu/gpu load the percent symbol gets clipped */
//#define noDegree

//------------------------------------- Other Stuff --------------------------------------------
/* comment out, to disable blank screen on serial timeout to retain info eg: PC crash fault diagnostics  */
#define enableActivityChecker

/* Time between "DisplayStyle" changes */
#define displayChangeDelay 18000

/* Timer for active connection to host*/
#define lastActiveDelay 10000

//------------------------------------ End of User configuration ---------------------------------

/*----------------------------------------------------------------------------------
  DEFINITIONS AND GLOBAL VARS
  ----------------------------------------------------------------------------------*/
/*SH1106 OLED setup*/
#ifdef OLED_SH1106
#include <Adafruit_SH1106.h>  // i2C not SPI
#define SH1106_128_64
#define OLED_RESET 4  //  "OLED_RESET" IS IS NOT A PHYSICAL PIN DO NOT CONNECT!!!
Adafruit_SH1106 display(OLED_RESET);
#endif

//-----------------------
/*SSD1306 OLED setup*/
#ifdef OLED_SSD1306
#include <Adafruit_SSD1306.h>  // i2C not SPI
#define SSD1306_128_64
#define SCREEN_WIDTH 128  // OLED display width, in pixels
#define SCREEN_HEIGHT 64  // OLED display height, in pixels
#define OLED_RESET 4      // "OLED_RESET" IS IS NOT A PHYSICAL PIN DO NOT CONNECT!!!
//#define OLED_RESET -1   //   QT-PY / XIAO
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
#endif

//----------------------
/* More OLED stuff*/
int oledDraw = 0;
int oledOverride = 0;
//----------------------
/* Inverted timers for oled*/
long invertDelay = 60000;  // 60 sec  delay
long lastInvertTime = 0;
int invertedStatus = 1;
//----------------------
/* Debounce timers for buttons  /// lastDebounceTime = millis();*/
long lastDebounceTime = 0;
long debounceDelay = 3000;
//----------------------
/* Delay screen event, to help stop screen data corruption ESP8622 use 25, most others 5 will do*/
int Serial_eventDelay = 0;

/* Timer for active connection to host*/
boolean activeConn = false;
long lastActiveConn = 0;
boolean bootMode = true;

/*vars for serial input*/
String inputString = "";
boolean stringComplete = false;
//----------------------
/* Start DisplayStyle */
int displayChangeMode = 1;
long lastDisplayChange;
//----------------------

/*----------------------------------------------------------------------------------
  SETUP
  ----------------------------------------------------------------------------------*/
void setup() {
    /* OLED SETUP */
#ifdef OLED_SH1106
    display.begin(SH1106_SWITCHCAPVCC, i2c_Address);  // initialize with the I2C addr 0x3D (for the 128x64)
#endif

#ifdef OLED_SSD1306
    display.begin(SSD1306_SWITCHCAPVCC, i2c_Address);  // initialize with the I2C addr 0x3D (for the 128x64
#endif

    display.clearDisplay();
    display.setTextColor(WHITE);

    display.setRotation(rotateScreen);  // Rotate the display at the start:  0, 1, 2 or 3 = (0, 90, 180 or 270 degrees)
    display.clearDisplay();

    /* stops text wrapping*/
    display.setTextWrap(false);  // Stop  "Loads/Temps" wrapping and corrupting static characters

    /* Serial setup, start serial port at 9600 bps and wait for port to open:*/
    Serial.begin(9600);  // 32u4 USB Serial Baud Rate
    inputString.reserve(200);

    /*Initial Load screen*/
    splashScreen();
}

//END of Setup

/*----------------------------------------------------------------------------------
  MAIN LOOP
  ----------------------------------------------------------------------------------*/

void loop() {
    /*Serial stuff*/
    serialEvent();

#ifdef enableActivityChecker
    activityChecker();
#endif

    /*change display screen*/
    if ((millis() - lastDisplayChange) > displayChangeDelay) {
        if (displayChangeMode == 1) {
            displayChangeMode = 2;
            display.fillRect(0, 0, 128, 64, BLACK);
        } else if (displayChangeMode == 2) {
            displayChangeMode = 3;
            display.fillRect(0, 0, 128, 64, BLACK);
        } else if (displayChangeMode == 3) {
            displayChangeMode = 4;
            display.fillRect(0, 0, 128, 64, BLACK);
        } else if (displayChangeMode == 4) {
            displayChangeMode = 1;
            display.fillRect(0, 0, 128, 64, BLACK);
        }

        lastDisplayChange = millis();
    }

    /* OLED DRAW STATS */
    if (stringComplete) {
        if (bootMode) {
            display.clearDisplay();
            display.display();
            bootMode = false;
        }

        lastActiveConn = millis();

        if (displayChangeMode == 1) {
            DisplayStyle1();
        } else if (displayChangeMode == 2) {
            DisplayStyle2();
        } else if (displayChangeMode == 3) {
            DisplayStyle3();
        } else if (displayChangeMode == 4) {
            DisplayStyle4();
        }

        inputString = "";
        stringComplete = false;

        /* Keep running anti screen burn, whilst serial is active */
        if ((millis() - lastInvertTime) > invertDelay && oledDraw == 1) {
            lastInvertTime = millis();

#ifdef enableInvertscreen
            inverter();
#endif
        }
    }
}

//END of Main Loop

/*----------------------------------------------------------------------------------
  FUNCTIONS
  ----------------------------------------------------------------------------------*/

//-------------------------------------------  Serial Events -------------------------------------------------------------

void serialEvent() {
    while (Serial.available()) {            //  32u4 USB Serial Available?
        char inChar = (char)Serial.read();  // Read 32u4 USB Serial

        inputString += inChar;
        if (inChar == '|') {
            stringComplete = true;
            delay(Serial_eventDelay);  //delay screen event to stop screen data corruption
            display.drawBitmap(118, 0, DownloadBMP, 10, 10, WHITE);// Update symbol in top right corner when updating
            display.display();
        }
    }
}

void activityChecker() {
    if (millis() - lastActiveConn > lastActiveDelay)
        activeConn = false;
    else
        activeConn = true;
    if (!activeConn) {
        if (invertedStatus)
            display.invertDisplay(0);

        //Turn off display when there is no activity
        display.clearDisplay();
        display.display();

        //Experimental,  attempt to stop intermittent screen flicker when in no activity mode "screen off" (due to inverter function?) fill the screen 128x64 black rectangle
        display.fillRect(0, 0, 128, 64, BLACK);
        display.display();
        oledDraw = 0;
    }
}

String getValue(String key) {
    String searchKey = key + ':';
    int keyIndex = inputString.indexOf(searchKey);
    if (keyIndex > -1) {
        int valIndex = inputString.indexOf("#", keyIndex);
        return inputString.substring(keyIndex + searchKey.length(), valIndex);
    }
}

String getCpuName() {
    String cpuName = getValue("CN");
    int cpuNameStart;
    if (cpuName.indexOf("Intel") > -1) {
        cpuNameStart = 6;
    } else {
        cpuNameStart = 4;
    }
    cpuName = cpuName.substring(cpuNameStart);

    if (cpuName.length() > 20) {
        return cpuName.substring(0, cpuName.lastIndexOf(" "));
    }
    else {
        return cpuName;
    }
   
}

String getGpuName() {
    String gpuName = getValue("GN");
    int gpuNameStart;
    if (gpuName.indexOf("NVIDIA") > -1) {
        gpuNameStart = 7;
    } else {
        gpuNameStart = 4;
    }
    return gpuName.substring(gpuNameStart);
}

//-------------------------------------------- Anti Screen Burn inverter ------------------------------------------------

void antiBurn() {
    display.invertDisplay(0);
    display.fillRect(0, 0, 128, 64, BLACK);
    display.display();
    oledDraw = 0;
}

void inverter() {
    if (invertedStatus == 1) {
        invertedStatus = 0;
    } else {
        invertedStatus = 1;
    };
    display.invertDisplay(invertedStatus);
    display.display();
}

//--------------------------------------------- Splash Screens --------------------------------------------------------
void splashScreen() {
    display.drawBitmap(0, 0, JustCatBMP, JustCatBMP_height, JustCatBMP_width, WHITE);
    display.setTextSize(2);
    display.setCursor(58, 5);
    display.println("Pikku");
    display.setTextSize(2);
    display.setCursor(64, 30);
    display.println("Stats");

    //Set version to USB Serial
    display.setTextSize(1);
    display.setCursor(68, 55);
    display.print("USB:v");
    display.print(CODE_VERS);

    display.display();
    delay(3000);
}
